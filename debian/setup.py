from setuptools import setup, find_packages

setup(
    name = "cctbx",
    version = "2021.12",
    author = "cctbx",
    author_email = "cctbx@cci.lbl.gov",
    description = "cctbx",
    license = "CCTBX 2.0",
    keywords = "crystallography",
    url = "http://cctbx.sourceforge.net/",
    packages=find_packages(
        where=".",
        exclude=["boost", "crys3d", "cudatbx", "gltbx", "prime", "xfel"],
    ),
    install_requires=[
        "h5py",
        "numpy",
        "matplotlib",
        # "_pycbf",
        "mrcfile",
        "pint",
        "procrunner",
        "pymol",
        "requests",
        "scipy",
        "six",
        "PIL",
        "tqdm",
        "wxPython",
    ],
)
